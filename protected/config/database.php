<?php

// This is the database connection configuration.
return array(
	'connectionString' => 'mysql:host=localhost;dbname=ariel',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'lani0363',
	'charset' => 'utf8',
	'initSQLs' => array("SET session time_zone = '-3:00';SET GLOBAL sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'"),
);