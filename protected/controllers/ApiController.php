<?php

class ApiController extends Controller
{
	private $body, $resp, $get, $post, $headers, $auth_token, $kk;

	public function missingAction($action)
	{
//		Helpers::qryExec("
//			insert into log(user_id, texto) values(10, :texto)
//		", array('texto'=>json_encode($_SERVER)));
		$this->chkRequest();
		$this->setRequestData();
		if ($action == 'id') {
			$this->id();
		} elseif (!in_array($action,['auth','imagenes','genera-menu','aplica', 'cierra', 'logout', 'login'])) {
			$this->chkSession();
		}
//		var_dump($this->body);
		$this->exec($action);
		return true;
	}

	protected function chkSession()
	{
		session_start();

		if ($this->auth_token !== session_id()) {
			$this->resp->sessionID = session_id();
			$this->resp->clienteID = $this->auth_token;
			$this->resp->status = 401;
			$this->resp->tokens = 'cliente:'.$this->auth_token.' - server:'.session_id();
			exit(json_encode($this->resp));
		} else {
			$this->resp->access_token = session_id();
			$this->resp->status = 200;
		};
	}

	private function setRequestData()
	{
//		var_dump(var_dump($HTTP_RAW_POST_DATA));die;
		$this->body = json_decode(file_get_contents('php://input'));
		$this->resp = new stdClass();
		$this->get = $_GET;
		$this->post = $_POST;
		$this->headers = apache_request_headers();
		$auth = isset($this->headers['authorization']) ? $this->headers['authorization'] : false;
		$auth = $auth ? $auth : (isset($this->headers['Authorization']) ? $this->headers['Authorization'] : 'no authorization in headers');
		$this->auth_token = $auth;
	}

	private function exec($action)
	{
		$actionFile = getcwd() . '/protected/controllers/api/' . $action . '.php';
		if (!file_exists($actionFile)) {
			throw new Exception('no existe ' . $actionFile);
		}
		require($actionFile);
	}

	private function chkRequest()
	{
		header('Access-Control-Allow-Origin: http://iae.dyndns.org');
		header('host:'.$_SERVER['HTTP_HOST']);
		if($_SERVER['HTTP_HOST'] === 'localhost'){
		 header('Access-Control-Allow-Origin: http://localhost:3000');
//			var_export($_SERVER);
		}
		header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_HOST']);
		header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
		if (array_key_exists('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', $_SERVER)) {
			header('Access-Control-Allow-Headers: '
				. $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
		} else {
//			header('Access-Control-Allow-Headers: *');
		}
		$port = $_SERVER['HTTP_HOST'] === 'localhost'?':3000':'';
		//vd('Access-Cont$this->bodyrol-Allow-Origin: http://'.$_SERVER['HTTP_HOST'].$port);
//		$host = substr($_SERVER['REMOTE_HOST']);
		if(isset($_SERVER['HTTP_ORIGIN'])){
			$host = $_SERVER['HTTP_ORIGIN'];
//		$host = $_SERVER['HTTP_ORIGIN'];
		}else{
			$host = 'http://localhost';
		}
		header('Access-Control-Allow-Origin: '.$host);

		if ("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
			exit(0);
		}
	}
}
