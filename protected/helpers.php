<?php
	function vd() {
		v(func_get_args());
		Yii::app()->end();
	}

		function ve() {
		v(func_get_args());
//    $args = func_get_args();
//    echo "<pre>";
//    var_export($args);
//    echo "</pre>";
	}

		function v($v) {
		foreach (func_get_args() as $v) {
			foreach ($v as $value) {
				// CVarDumper::dump($value, 100, true);
				var_dump($value);
			}
		}
	}
function qryAllOrEmpty($qry, $params = array()) {
			$qry = Yii::app()->db->pdoInstance->query($qry);
			$qry->setFetchMode(PDO::FETCH_ASSOC);
			try {
				$data = $qry->fetchAll();
			} catch (Exception $e) {
				echo "<pre>";
				echo "Error:" . $e->getCode() . "</br>";
//      if ($e->getCode() == 42) {
//        echo 'Error ejecutando query:</br>';
				ve($e->getMessage());
//      }
				echo $e->getTraceAsString();
				echo "</pre>";
				die;
			}
			if ($qry->rowCount() == 0) {
				foreach (range(0, $qry->columnCount() - 1) as $column_index) {
					$meta = $qry->getColumnMeta($column_index);
					$fields[$meta["name"]] = "";
				}
				return array($fields);
			} else {
				return $data;
			}
		}
function qryAll($qry, $params = array(), $conn = null) {
			$conn = $conn ? $conn : Yii::app()->db;
			$conn = $conn ? $conn : Yii::app()->db;
			try {
				$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
				return $conn->createCommand($qry)->queryAll(true, $params);
			} catch (Exception $e) {
				echo "<pre>";
				echo "Error:" . $e->getCode() . "</br>";
//      if ($e->getCode() == 42) {
//        echo 'Error ejecutando query:</br>';
				ve($e->getMessage());
//      }
				echo $e->getTraceAsString();
				echo "</pre>";
				die;
			}
			return null;
		}
function qry($qry, $params = array()) {
			try {
				return Yii::app()->db->createCommand($qry)->query($params)->read();
			} catch (Exception $e) {
				echo "<pre>";
				//if (in_array($e->getCode(),array(42,42000))) {
				echo 'Error ejecutando query:</br>';
				ve($e->getCode() . '-' . $e->getMessage());
				//}
				echo $e->getTraceAsString();
				echo "</pre>";
				die;
			}
			return null;
		}